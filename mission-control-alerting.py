#!/usr/bin/env python3

import argparse
import sys
import json
import operator
from datetime import datetime, timedelta

def uniqueSatelliteIds(list):
    """Returns a list of the unique satellite IDs that are input via a list upon calling the function."""
    
    unique_out = []
    for x in list:
        if x.satellite_id not in unique_out:
            unique_out.append(x.satellite_id)
    return unique_out

def jsonAlert(alert,limit_type):
    """Outputs an alert to the console in JSON format with indentation."""
    
#    print(json.dumps({"satelliteId":alert.satellite_id,"severity":((limit_type.replace("_limit","")).replace("_"," ")).upper(),"component": alert.component,"timestamp": "Z".join(alert.timestamp.isoformat().rsplit(alert.timestamp.isoformat()[-3:],1))},indent=4))
    return ({"satelliteId":alert.satellite_id,"severity":((limit_type.replace("_limit","")).replace("_"," ")).upper(),"component": alert.component,"timestamp": "Z".join(alert.timestamp.isoformat().rsplit(alert.timestamp.isoformat()[-3:],1))})

class SatRecord:
    """Parses the line that is input to allow further filtering and comparison."""
    
    def __init__(self,line):
        self.timestamp = datetime.strptime(line.split('|')[0], '%Y%m%d %H:%M:%S.%f')
        self.satellite_id = int(line.split('|')[1])
        self.red_high_limit = int(line.split('|')[2])
        self.yellow_high_limit = int(line.split('|')[3])
        self.yellow_low_limit = int(line.split('|')[4])
        self.red_low_limit = int(line.split('|')[5])
        self.raw_value = float(line.split('|')[6])
        self.component = str(line.split('|')[7])
    
    def __repr__(self):    
        return "\n%r" % (self.__dict__)

class mcAlerting:
    """
    This class will handle the file ingestion and requisite conversion to the SatRecord object type, and the handling of the alerts.
    
    ...
    
    Attributes
    ----------
    telementrydata : list
        Contains all incoming messages from satellites.
    satellite_ids_unique : list
        Contains the unique satellite IDs in a list.
        
    Methods
    -------
    process(component,limit_type,comparison_type,interval,allowance)
        Processes the alerts in telemetrydata according to the specified filters.
    """
    
    def __init__(self,fileContent):
        """
        Args:
            fileContent : TextIOWrapper
                The message log from the satellites.
        """
        self.telemetrydata = [SatRecord(line.strip()) for line in fileContent]
        self.satellite_ids_unique = uniqueSatelliteIds(self.telemetrydata)

    def __repr__(self):
        return "%r" % (self.__dict__)
    
    def process(self,component,limit_type,comparison_type,interval,allowance):
        """Processes the alerts in telemetrydata according to the specified filters.

        Args:
            component : str
                The component that will need to be filtered against.
            limit_type : str
                The limit type from the SatRecord entry that is being compared to the raw value from the SatRecord entry
            comparison_type : str
                Type of comparison to be made, gt (>), gte (>=), lt (<), lte (<=)
            interval : int
                Amount of time in minutes for the allowance of repeated violations from the first instance.
            allowance : int
                Number of times a repeated violation can occur.
        """
        if comparison_type == 'gt':
            self.alerts = [x for x in self.telemetrydata if (x.component == component) and (x.raw_value > eval('x.{0}'.format(limit_type)))]
        elif comparison_type == 'gte':
            self.alerts = [x for x in self.telemetrydata if (x.component == component) and (x.raw_value >= eval('x.{0}'.format(limit_type)))]
        elif comparison_type == 'lt':
            self.alerts = [x for x in self.telemetrydata if (x.component == component) and (x.raw_value < eval('x.{0}'.format(limit_type)))]
        elif comparison_type == 'lte':
            self.alerts = [x for x in self.telemetrydata if (x.component == component) and (x.raw_value <= eval('x.{0}'.format(limit_type)))]
        self.alerts.sort(key = operator.attrgetter('timestamp'))
        self.start_time = self.alerts[0].timestamp
        self.alert_return = [x for x in self.alerts if (x.timestamp - self.start_time) <= timedelta(minutes=interval)]
        if len(self.alert_return) >= allowance:
            return jsonAlert(self.alerts[0],limit_type)

def main(arguments):
    """
    Processes the given file against the defined alert thresholds.

    Args:
        file : TextFile
            Pipline deliniated file that containers data in "<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>" format.
        outfile : str
            Output file if testing alert data.  Data will be writen as JSON.
    """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("-f","--file",help='Pipline deliniated file that containers data in "<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>" format.',type=argparse.FileType('r'),required=True)
    parser.add_argument("-o","--outfile",help='Output file for validating JSON output.',type=str)
    args = parser.parse_args()
    
    alerts = mcAlerting(args.file)
    alertout = []
    alertout.append(alerts.process("TSTAT","red_high_limit",'gt',5,3))
    alertout.append(alerts.process("BATT","red_low_limit",'lt',5,3))
    
    if args.outfile is None:
        print(json.dumps(alertout,indent=4))
    else:
        with open(args.outfile, "w") as outfile:
            json.dump(alertout,outfile,indent=4)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))